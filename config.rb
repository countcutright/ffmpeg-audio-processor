require 'etc'

$ffmpeg = "C:\\Program Files (x86)\\Audacity\\ffmpeg\\ffmpeg.exe"

$copy_nonmatching_files = true # copy over all files not re-encoded (see $formats and $copy_blacklist_formats)
$src_directory = ARGV[0] # if args given on command line
$default_directory = Dir.pwd # if no args given on command line

# encodes @ quality if bitrate > threshold
# otherwise it re-encodes using source bitrate approximated as CBR (ie, 128k, 221k, etc.)
# valid bitrates: 8,16,24,32,40,48,64,80,96,112,128,160,192,224,256,320
bitrate_cbr = 192
samplerate = 44100 # default: 44100 Hz -> see https://en.wikipedia.org/wiki/Nyquist_frequency if you think this should change...
$ffmpeg_encoding_quality = "-ab #{bitrate_cbr}k" # note that we can also use other encoding types: -a 0, etc.
$ffmpeg_encoding_samplerate = "-ar #{samplerate}"
$threshold_samplerate = samplerate + 400 # in Hz, default: +400 Hz
$threshold_bitrate = 1.1*bitrate_cbr # in kb/s, default: 110% of bitrate
$strip_discnumber = true # strip the "disc" field of the metadata

# target RMS in dB, set to 0 to disable this feature
# disabled = loud tracks stay loud, quiet tracks amplified so that their peak is at 0db
$target_rms = -13  # default: -13 dB, EXPERIMENTAL (around -11 results in 0db)
# will it clip when amplifying tracks? if true, will forcibly amplify quiet tracks up to $target_rms, 
#    otherwise, they will be amplified so that their peak is at 0dB or $target_rms, whichever is less
$allow_clipping = false  # default: false

# if true, will overwrite atomically
# if false, creates "../$top_level_folder ($output_folder)" for output
#$output_to_same_folder = false #TODO - implement me
$output_folder = "processed " + $ffmpeg_encoding_quality[$ffmpeg_encoding_quality.index(/\d/)..-1]
$threads = Etc.nprocessors - 1
$threads = 1 if $threads <= 0
$progressbar_enabled = true # progresss bar with ETA (should probably disable for benchmarking)
$windowtitle_enabled = true # updates the window title with information

# debug info
$show_debug = false 
$show_errors = true
$log_traversal = false # adds a lot of overhead, shows the tree being built
$log_to_file = true
$overwrite_log_file = true # append or overwrite debug log file (the failures file is always overwritten if there are failures)
$debug_log_file = "debug.log"
$failures_log_file = "failures.log"

# $formats: formats for files to try to re-encode
$formats = [".mp3", ".wav", ".flac", ".m4a"]
# $copy_blacklist: formats to exclude from copying if $copy_nonmatching = true
$copy_blacklist_formats = [".jpg", ".jpeg", ".png", ".ini", ".txt"]

