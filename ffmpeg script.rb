# @Author Aaron Cutright
#
# ffmpeg music batch processing script: meant to be used to re-encode huge libraries of music
#
# -------- USAGE ---------------
# edit the config options in 'config.rb'
# all other files should be left alone, unless you're a developer
#
require 'open3'
require 'parallel'
require 'benchmark'
require 'FileUtils'
require 'ruby-progressbar'
require 'time'
require_relative 'config'
require_relative 'helpers'

$output_format = ".mp3"
$codec = "-codec:a libmp3lame"
$duration_margin_of_error = 2 # used to check for encoding consistency, specifies acceptable discrepency in seconds
$encoding_retry_limit = 3 # number of times to try to encode something before declaring it an error

#do not touch these
$ffmpeg_encoding_flags = "#{$codec} -map_metadata 0 -id3v2_version 3 -metadata comment= -metadata COMMENT= -metadata URL= -metadata LOGFILE= -metadata url= -metadata logfile= -metadata TRACK= "
$ffmpeg_detection_flags = "-filter:a astats -vn -f null NUL"
$ffmpeg_strip_replaygain = "-metadata replaygain_album_gain= -metadata replaygain_album_peak= -metadata replaygain_track_gain= -metadata replaygain_track_peak= -metadata REPLAYGAIN_ALBUM_GAIN= -metadata REPLAYGAIN_ALBUM_PEAK= -metadata REPLAYGAIN_TRACK_GAIN= -metadata REPLAYGAIN_TRACK_PEAK= "
$progress = 0.0
$silence_db = -999999999 # used as placeholder for 'silence' in dB

# build the file tree 
def buildFileList(inroot_g, outroot_g)
	debug "building list of files to work with..."
	def buildList(inroot, outroot, inpaths, outpaths)
		if not $output_to_same_folder and not File.directory?(outroot)
			Dir.mkdir(outroot)
		end
		Dir.foreach(inroot, encoding:"utf-8") do |x|
			inpath = File.join(inroot, x)
			outpath = File.join(outroot, x)
			if ($log_traversal)
				debug "traversing \"#{inpath}\""
			end
			if x == "." or x == ".."
				next
			elsif File.directory?(inpath)
				if not File.directory?(outpath)
					Dir.mkdir(outpath)
				end
				buildList(inpath, outpath, inpaths, outpaths)
			else
				copied = false
				$formats.each do |ftype|
					if x.end_with?(ftype)
						if $output_to_same_folder and not Dir.exist?($output_folder)
							Dir.mkdir($output_folder)
						end
						filename = x[0..x.rindex(ftype)-1]
						outpath = File.join(outroot, filename+$output_format)
						inpaths.push(inpath)
						outpaths.push(outpath)
						copied = true
						break
					end
				end
				if copied == false and $copy_nonmatching_files == true
					copy = true
					$copy_blacklist_formats.each do |ftype|
						if x.end_with?(ftype)
							copy = false
							break
						end
					end
					if copy == true
						debug "copying \"#{inpath}\""
						FileUtils.cp(inpath, outpath)
					else
						debug "not copying \"#{inpath}\""
					end
				end
			end
		end
	end
	debug "made new directory structure for output: '#{outroot_g}', based on '#{inroot_g}'"
	inpaths = []
	outpaths = []
	buildList(inroot_g, outroot_g, inpaths, outpaths)
	return inpaths, outpaths
end

# get the metadata for the file at 'filepath'
# @returns (bitrate, duration, size, max_peak)
def get_metadata(filepath)
	# get peak levels and bitrate info for file
	cmd = "\"#{$ffmpeg}\" -i \"#{filepath}\" #{$ffmpeg_detection_flags}"
	debug "peak/bitrate command: \"#{cmd}\""
	stdout, stderr, status = Open3.capture3(cmd)
	# prune stdout & stderr
	input_idx = stdout.index('Input #0')
	output_idx = stdout.index('Output #0')
	result_idx = stdout.rindex('Press [q] to stop, [?] for help')
	if (not input_idx.nil?) && (not output_idx.nil?) && (not result_idx.nil?)
		stdout = "#{stdout[input_idx..output_idx]}#{stdout[result_idx..-1]}"
	end
	input_idx = stderr.index('Input #0')
	output_idx = stderr.index('Output #0')
	result_idx = stderr.rindex('Press [q] to stop, [?] for help')
	if (not input_idx.nil?) && (not output_idx.nil?) && (not result_idx.nil?)
		stderr = "#{stderr[input_idx..output_idx]}#{stderr[result_idx..-1]}"
	end
	results = "#{stdout}#{stderr}"

	# find title and track number
	tracknumber = nil
	begin 
		tracknumber = results.scan_with_error(/^\s*track\s*: (\d+)/i)[0][0].to_i
	rescue; end

	# get duration/bitrate/samplerate information
	bitgrp = results.scan_with_error(/^\s*Duration:.*bitrate: (\d+.?\d*) kb\/s.*[^$]*.*Stream.*Audio: \w+, (\d+.?\d*) Hz/)[0]
	bitrate = bitgrp[0].to_f 
	samplerate = bitgrp[1].to_f
	
	# skip to duration
	info = results[results.rindex('Press [q] to stop, [?] for help')..-1]
	info = info[0..info.index('muxing overhead')]
	info = info[info.rindex('time=')..-1]
	# get duration
	durstr = info[info.index(/\d/)..info.index(' bitrate=')-1]
	duration = Time.parse(durstr).to_i - Time.parse('00:00').to_i

	# find peak volume to normalize to
	peak = $silence_db
	peak = results.scan_with_error(/^.*Peak level dB: (-?\d+.?\d+)/).last[0].to_f

	# find approximate volume of song (to adjust down if needed)
	rms = $silence_db
	rms_levels = results.scan_with_error(/^.*RMS level dB: (-?\d+.?\d+)/)
	rms_levels.reverse_each do |rmsstr|
		rmsstr = rmsstr[0]
		if (not rmsstr.blank?)
			rms = max(rms, rmsstr.to_f)
		end
	end

	debug "[metadata]\n"
		.concat(" file = '#{filepath}'\n")
		.concat("  tracknumber = #{tracknumber}\n")
		.concat("  samplerate = #{samplerate} Hz \n")
		.concat("  bitrate = #{bitrate} kb/s \n")
		.concat("  max_peak = #{peak} dB \n")
		.concat("  rms = #{rms} dB\n")
		.concat("  duration = #{duration} secs \n")
	return samplerate, bitrate, duration, peak, rms, tracknumber
end


# @returns data to verify encoding was correct
def get_encoding_checks(filepath)
	# ensure that file exists
	raise FileNotFoundException, "for #{filepath}" if (not File.file? filepath)

	# use ffmpeg to query for file info
	cmd = "\"#{$ffmpeg}\" -i \"#{filepath}\" #{$ffmpeg_detection_flags}"
	debug "check command: \"#{cmd}\""
	stdout, stderr, status = Open3.capture3(cmd)
	results = "#{stdout}#{stderr}"

	# skip to the results of running the command
	results = results[results.rindex('Press [q] to stop, [?] for help')..-1]
	results = results[0..results.index('muxing overhead')]

	# skip to duration
	results = results[results.rindex('time=')..-1]
	# get duration
	durstr = results[results.index(/\d/)..results.index(' ')-1]
	duration = Time.parse(durstr).to_i - Time.parse('00:00').to_i

	debug "[checks]\n"
		.concat(" file = '#{filepath}'\n")
		.concat("  duration = #{duration} secs \n")
	return duration
end

$failure_paths = []
# encode all the files in inpaths, output to outpaths
def encode_all(inpaths, outpaths)
	percent_complete = 0
	percent_per_file = 100.0/inpaths.size
	$failure_paths = []
	#, progress: "encoding..."
	Parallel.each_with_index(inpaths, in_threads: $threads, progress: ($progressbar_enabled ? "encoding..." : false)) do |inpath, index|
		infilename = inpath[max(inpath.rindex('/'), inpath.rindex('\\'))+1..-1]
		outpath = outpaths[index]
		encoding_success = false
		for attempt_num in 1..$encoding_retry_limit do
			debug "Processing \"#{inpath}\"..."

			samplerate, bitrate, duration_in, max_peak, rms, tracknumber = get_metadata(inpath)
			new_peak = 0
			adjust = false
			# if song is too loud, de-amplify it
			if (rms < 0 && rms > $target_rms)  
				debug "song is too loud... rescaling to #{$target_rms} dB"
				new_peak = -(rms - $target_rms) # de-amplify to rms threshold
				adjust = true
			# if song is too quiet, amplify it
			elsif (max_peak < 0 and max_peak > $silence_db) 
				debug "song is too quiet... allow clipping? #{($allow_clipping ? "true" : "false")}... rescaling to #{$target_rms} dB"
				if ($allow_clipping && rms < 0 && rms > $silence_db)
					new_peak = -(rms - $target_rms) # amplify to rms threshold
				else
					new_peak = -max_peak # amplify so peak is at 0dB
				end
				adjust = true
			end

			# add ffmpeg flags
			encoding_flags = "" + $ffmpeg_encoding_flags # create new string
			if tracknumber != nil 
				trackstr = "\"#{(tracknumber < 10)? "0#{tracknumber.to_s}" : tracknumber.to_s}\""
				encoding_flags.concat(" -metadata track=#{trackstr}")
			end
			if adjust 
				encoding_flags.concat(" -af \"volume=#{new_peak}dB\" #{$ffmpeg_strip_replaygain}")
			end
			if $strip_discnumber
				encoding_flags.concat(" -metadata disc= ")
				debug "stripping disc number for #{inpath}"
			end
			# decrease sampling rate if needed
			if (samplerate > $threshold_samplerate) or (samplerate == 0)
				encoding_flags.concat(" #{$ffmpeg_encoding_samplerate}")
			end
			# decrease bitrate if needed
			if (bitrate > $threshold_bitrate) or (bitrate == 0)
				encoding_flags.concat(" #{$ffmpeg_encoding_quality}")
			else
				# need some bitrate checks to make sure not to rescale it (usually bitrate is imperfect because of variable-encodings)
				if bitrate < 12
					encoding_flags.concat(" -b:a 8k")
				elsif bitrate < 20
					encoding_flags.concat(" -b:a 16k")
				elsif bitrate < 28
					encoding_flags.concat(" -b:a 24k")
				elsif bitrate < 36
					encoding_flags.concat(" -b:a 32k")
				elsif bitrate < 44
					encoding_flags.concat(" -b:a 40k")
				elsif bitrate < 56
					encoding_flags.concat(" -b:a 48k")
				elsif bitrate < 72
					encoding_flags.concat(" -b:a 64k")
				elsif bitrate < 88
					encoding_flags.concat(" -b:a 80k")
				elsif bitrate < 104
					encoding_flags.concat(" -b:a 96k")
				elsif bitrate < 120
					encoding_flags.concat(" -b:a 112k")
				elsif bitrate < 150
					encoding_flags.concat(" -b:a 128k")
				elsif bitrate < 182
					encoding_flags.concat(" -b:a 160k")
				elsif bitrate < 214
					encoding_flags.concat(" -b:a 192k")
				elsif bitrate < 246
					encoding_flags.concat(" -b:a 224k")
				elsif bitrate < 310
					encoding_flags.concat(" -b:a 256k")
				else
					encoding_flags.concat(" -b:a 320k")
				end
			end
			# re encode mp3
			cmd = "\"#{$ffmpeg}\" -i \"#{inpath}\" #{encoding_flags} \"#{outpath}\""
			debug "encode command: \n  #{cmd}\n"
			stdout, stderr, status = Open3.capture3(cmd)

			# check encoded file for errors, otherwise try again
			begin
				duration_out = get_encoding_checks(outpath)
				if ((duration_out - duration_in).abs <= $duration_margin_of_error)
					encoding_success = true
				end
			rescue
				if (attempt_num < $encoding_retry_limit)
					debug_error "encoding failed... attempt num #{attempt_num}"
				end
			end
			break if encoding_success
		end
		if (encoding_success)
			debug "encoding success => '#{outpath}'"
			percent_complete += percent_per_file
			if $windowtitle_enabled then
				system("title #{(percent_complete).round(2)}\% done (finish #{index}/#{inpaths.size}=\"#{infilename}\")")
			end
		else
			debug_error "encoding FAILED for '#{inpath}'"
			$failure_paths.push(inpath)
		end
	end 
	if $windowtitle_enabled then
		system("title Encoding completed")
	end
end




# entry point

if $overwrite_log_file and $log_to_file
	File.open($debug_log_file, 'w'){ |file| file.puts "" }
end

if $src_directory.nil? or (not File.directory?($src_directory))
	$src_directory = $default_directory
end

output "output format: #{$output_format}, codec: #{$codec}"
output "target bitrate: #{$ffmpeg_encoding_quality}, samplerate: #{$ffmpeg_encoding_samplerate}"
output "target rms: #{$target_rms} dB, allow clipping? #{$allow_clipping? "true":"false"}"
output "using #{$threads} threads"
output "log file is \"#{$debug_log_file}\"" if $log_to_file
output "encoding all in \"#{$src_directory}\" => \"#{File.expand_path($src_directory)+' ('+$output_folder+')'}\""

num_files = 0
# for re-encoding a single folder
if not $output_to_same_folder 
	inpaths, outpaths = buildFileList($src_directory, File.expand_path($src_directory)+" (#{$output_folder})")
	num_files = inpaths.size
	output "encoding #{num_files} files\n\n"
	encode_all(inpaths, outpaths)
else
	inpaths, outpaths = buildFileList($src_directory, $src_directory)
	num_files = inpaths.size
	output "encoding #{num_files} files\n\n"
	encode_all(inpaths, outpaths)
end 

# report failed encodings
output "" # newline
if $failure_paths.size == 0
	output "all encodings were successful"
else
	output "successfully encoded #{num_files - $failure_paths.size}/#{num_files}, failures logged to \"#{$failures_log_file}\". See \"#{$debug_log_file}\" for debugging."
	File.open($failures_log_file, 'w') do |file|
		$failure_paths.each do |failure_path|
			file.puts failure_path
		end
	end
end

# for benchmarking threads or whatever else you might want
#Benchmark.bm(25) do |bm|
#	$output_to_same_folder = false
#	$logging_enabled = false
#	$progressbar_enabled = false
#	(1..10).each do |i|
#		bm.report("#{i} threads:"){
#			$output_folder = "benchmark #{i}"
#			$threads = i
#			inpaths, outpaths = buildFileList($src_directory, File.expand_path($src_directory)+" (#{$output_folder})")
#			encode_all(inpaths, outpaths)
#		}
#	end
#end

# thread count speedups on an i7-4770 on a 271MB folder of 37 256k files -> 192k
#                                user     system      total        real
#1 threads:                  0.109000   0.140000   0.249000 (230.570295)
#2 threads:                  0.078000   0.078000   0.156000 (117.522705)
#3 threads:                  0.078000   0.063000   0.141000 ( 80.290518)
#4 threads:                  0.110000   0.203000   0.313000 ( 67.827948)
#5 threads:                  0.078000   0.172000   0.250000 ( 60.920997)
#6 threads:                  0.062000   0.156000   0.218000 ( 56.356748)
#7 threads:                  0.110000   0.109000   0.219000 ( 52.003718)
#8 threads:                  0.078000   0.219000   0.297000 ( 49.538919)
#9 threads:                  0.140000   0.110000   0.250000 ( 49.830435)
#10 threads:                 0.016000   0.171000   0.187000 ( 50.078468)
# output was 206MB

# results for encoding a 24.5 GB folder of 3571 files (2719 tracks, 10:28:03.91 time) of 160k-320k (avg. 281k)
#
#flags:  -codec:a libmp3lame -map_metadata 0 -id3v2_version 3
#target bitrate:  -b:a 192k
#using 6 threads
#encoding... |Time: 01:19:40 | ================================= | Time: 01:19:40
# output size was 18.8 GB, 2721 files (avg. 188k)
# size reduction: 23.3%
#
#flags:  -codec:a libmp3lame -map_metadata 0 -id3v2_version 3
#target bitrate:  -b:a 224k
#using 6 threads
#encoding... |Time: 01:19:30 | ================================= | Time: 01:19:30
# output size was 21 GB, 2721 files (avg. 213k)
# size reduction: 14.3%