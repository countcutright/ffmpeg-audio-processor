# @Author Aaron Cutright
#
# playlist_repair.rb: Mostly automated way to fix playlist mappings if you change folders around
# - Only works on m3u/m3u8 playlists, this is out of laziness
# - The driving logic for 'is this the same file' boils down to the edit distance on file paths
#
# -------- USAGE ---------------
# playlist_repair.rb 'playlist.m3u'
#
require 'FileUtils'
require 'etc'
require 'pathname'
require 'ruby-progressbar'

$music_root = "E:/Windoze/Music" # is there a way to make windows report this? what about linux?
$blacklist_dirs = ["E:/Windoze/Music/(do not copy over -- todo)"]

# output options
$output_relative_paths = true # otherwise, outputs absolute paths into m3u8
$dir_separator = '/' # for the output file

# file types
$allowed_playlist_types = [".m3u", ".m3u8"]
$allowed_filetypes = [".mp3", ".flac", ".wma", ".m4a"]

# Many programs mistakenly handle unknown characters as '?'. Set this to true and '?'
# will be treated as a wildcard (but the final playlist will have valid utf-8 paths,
# so junk in -> good out)
$treat_questionmark_as_encoding_error = true 

# logging options
$show_debug = false # print to stdout
$show_errors = true # print to stderr
$log_to_file = true # log all debug and error messages to the file
$log_errors_to_separate_file = true 
$overwrite_log_file = true # clear the log file when this script is initialized
$log_file_relative = true # true = log file will be written relative to the input file path, false = relative to this script 
$log_file_name_after_playlist = true # overrides the name of $log_file, to match the output playlist name but ending with .log
$log_file_name = "debug.log"
$error_file_name = "errors.log"
$output_suffix = "_fixed" # suffix to add to the output folder, output file names will be '{input_folder_name}{output_suffix}/{input_file_name}.m3u8'

# detailed logging options
$log_traversal = false # log the traversal of  the entire music directory -- only use this for debugging, it wrecks performance

# logging functions
def output(args)
	args = args.to_s
	$stdout.puts args
	if $log_to_file
		File.open($log_file, 'a:UTF-8'){|file| file.puts args}
	end
end
def debug(args)
	args = args.to_s
	if $show_debug
		$stdout.puts "[DEBUG] ".concat(args)
		$stdout.flush
	end
	if $log_to_file
		File.open($log_file, 'a:UTF-8'){|file| file.puts args}
	end
end
def error(args)
	args = args.to_s
	if $show_errors
		$stderr.puts "[ERR]   ".concat(args)
		$stderr.flush
	end
	if $log_to_file
		if $log_errors_to_separate_file
			File.open($error_file, 'a:UTF-8'){|file| file.puts args}
		end
		# log errors to debug file as well
		File.open($log_file, 'a:UTF-8'){|file| file.puts args}
	end
end

# extension functions
class Object
	def blank?
		return true if (self.nil? or self.empty?)
		return self.strip.empty? if defined? self.strip 
		if defined? self.each
			self.each do |obj|
				return false if not obj.blank?
			end
			return true
		end
		return false
	end
end

class String
	def relative_path_from(basepath)
		self.to_pathname.relative_path_from(basepath.to_pathname)
	end

	def to_pathname
		Pathname.new self
	end

	def force_utf8
		self.force_encoding('utf-8').encode('utf-8', :invalid => :replace)
	end

	def strip_utf8
		line = ''
		begin
			line = self.strip.gsub(/\A\p{Space}+|\p{Space}+\z/, '') #strip begin/end whitespace (incl. unicode)
		rescue => e
			error "Something is wrong with this line: \"#{line}\""
			error e.backtrace.to_s
			raise # re-raise the exception
		end
		line
	end

	def eq_with_broken_utf8?(toCheck)
		return true if self == toCheck
		if $treat_questionmark_as_encoding_error
			str1 = self
			str2 = toCheck
			if not (str1.includes_extended_utf8? or str2.includes_extended_utf8?)
				return false if str1.length != str2.length
			end
			shorter_length = (str1.length < str2.length)? str1.length : str2.length
			for i in 0..shorter_length-1
				# english/symbol: 0x21..0x7e, non-ascii are > 0x7e
				char1 = str1[-i]
				char2 = str2[-i]
				return false if char1 != char2 and char1 != '?' and char2 != '?'
			end
			return true
		end
		return false
	end

	def includes_extended_utf8?
		return true if self.include? '?'
		for i in 0..self.length-1
			chr = self[i]
			return true if chr.ord > 0x7e
		end
		return false
	end

	#src: wikipedia
	def longest_common_substring(str)
		return '' if [self, str].any?(&:empty?)

		matrix = Array.new(self.length) { Array.new(str.length) { 0 } }
		intersection_length = 0
		intersection_end = 0
		self.length.times do |x|
			str.length.times do |y|
				next unless self[x].eq_with_broken_utf8?(str[y]) # note that we ignore when '?', count as ==
				matrix[x][y] = 1 + (([x, y].all?(&:zero?)) ? 0 : matrix[x-1][y-1])

				next unless matrix[x][y] > intersection_length
				intersection_length = matrix[x][y]
				intersection_end = x
			end
		end
		intersection_start = intersection_end - intersection_length + 1
		slice(intersection_start..intersection_end)
	end

	# levenshtein distance
	# src: https://github.com/rubygems/rubygems/blob/master/lib/rubygems/text.rb
	def edit_distance(str)
    n = self.length
    m = str.length
    return m if (0 == n)
    return n if (0 == m)

    d = (0..m).to_a
    x = nil
    self.each_char.each_with_index do |char1, i|
      e = i + 1
      str.each_char.each_with_index do |char2, j|
        cost = (char1.eq_with_broken_utf8?(char2))? 0 : 1
        x = [d[j + 1] + 1, # insertion
             e + 1,        # deletion
             d[j] + cost   # substitution
        ].min
        d[j] = e
        e = x
      end
      d[m] = x
    end
    return x
	end
end

# lower = better
def path_similarity_ranking(str1, str2)
	# shorter_length = str2.length
	# longer_length = str1.length
	# if str1.length < str2.length
	# 	shorter_length = str1.length
	# 	longer_length = str2.length
	# end
	# lcs = str1.longest_common_substring(str2)
	# lcs_delta = longer_length - lcs.length
	lev_dist = str1.edit_distance(str2)
	
	#return lcs_delta + (lev_dist ** 2)
	return lev_dist
end

# check for equality, considering the extended utf-8 to '?' problem
def filenames_eq?(path1, path2)
	f1 = File.basename(path1)
	f2 = File.basename(path2)
	return f1.eq_with_broken_utf8?(f2)
end


# build the file tree 
def buildFileList(music_root)
	def buildList(music_root, music_list)
		Dir.foreach(music_root, encoding:"utf-8") do |filename|
			path = File.join(music_root, filename)
			debug "traversing \"#{path}\", at file \"#{filename}\"" if $log_traversal
			if filename == "." or filename == ".."
				next
			elsif File.directory?(path)
				buildList(path, music_list) if not $blacklist_dirs.include?(path)
			else
				filetype = File.extname(filename).downcase
				music_list.push(path) if $allowed_filetypes.include?(filetype)
			end
		end
	end
	music_list = []
	buildList(music_root, music_list)
	debug "" if $log_traversal
	return music_list
end


def fix_playlist(playlist_path)
	input_playlist = playlist_path.gsub('\\','/').to_s # fix windows paths
	input_format = File.extname(input_playlist).downcase
	if not ($allowed_playlist_types.include? input_format)
		raise 'Only works with playlist types: ' + $allowed_playlist_types.to_s
	end

	parent_path = File.expand_path('../..', input_playlist)
	input_dirname = File.basename(File.dirname(File.expand_path(input_playlist)))
	playlist_dir = "#{parent_path}/#{input_dirname}#{$output_suffix}/"
	Dir.mkdir playlist_dir if not Dir.exist? playlist_dir
	output_playlist = playlist_dir + File.basename(input_playlist, ".*") + input_format
	output_playlist = File.expand_path(output_playlist)

	debug "" # add newline between playlists in the debug file
	output "fixing playlist \"#{File.basename(input_playlist)}\" => \"#{output_playlist}\""

	output_lines = []
	Dir.chdir(playlist_dir) do
		local_vars = [playlist_dir, input_format, output_lines]
		def fix_by_lines(lines, encoding, local_vars)
			# scoping hack
			playlist_dir, input_format, output_lines = local_vars

			for index in 0..lines.length-1
				line = ""
				if (encoding == "windows-1252")
					line = lines[index].force_encoding('windows-1252').encode('utf-8').strip
				elsif (encoding == "utf-8")
					line = lines[index].force_utf8.strip_utf8
				else
					raise 'Do not understand encoding'
				end
				line = line.gsub('\\','/').gsub('ï»¿','')

				local_vars = [playlist_dir, input_format, output_lines, encoding, lines, index]
				def process_line(line, tried_alternate_encoding, local_vars)
					# scoping hack
					playlist_dir, input_format, output_lines, encoding, lines, index = local_vars
					if (index == 0)
						if (line.include? '#')
							e1 = line.upcase
							e1 = e1[e1.index('#')..-1] # in case of invalid characters, which are common
							if (e1 != '#EXTM3U' and e1 != '#EXTM3U8')
								error "only supports m3u playlists ('#EXTM3U' or '#EXTM3U8' header)"
								error "line[#{index}]='{line}'"
							elsif (input_format == '.m3u')
								output_lines.push '#EXTM3U'
								return
							elsif (input_format == '.m3u8')
								output_lines.push '#EXTM3U8'
								return
							else
								output_lines.push '#EXTM3U8' 
								return
							end
						end
						if (input_format == '.m3u')
							output_lines.push '#EXTM3U'
						elsif (input_format == '.m3u8')
							output_lines.push '#EXTM3U8'
						else
							output_lines.push '#EXTM3U8' 
						end
					end
					absolute_path = File.expand_path(line).to_s.gsub('\\', '/')
					relative_path = (absolute_path.relative_path_from playlist_dir).to_s.gsub('\\', '/')

					if File.exist? line
						path = $output_relative_paths? relative_path : absolute_path
						path = path.gsub('/', $dir_separator)
						output_lines.push path
						return
					end
					
					filename = File.basename(line)
					# find the closest match, with file equality
					best_match = nil
					best_match_ranking = 100000 # if the edit distance were this bad, it wouldn't matter...
					$music_list.each do |music_path|
						if filenames_eq?(File.basename(music_path), filename)# or (File.basename(music_path).edit_distance(filename) < 2)
							# make both paths relative
							path = relative_path
							music_path = (music_path.relative_path_from playlist_dir).to_s
							# now compare them
							ranking = path_similarity_ranking(path, music_path)
							if ranking < best_match_ranking
								best_match_ranking = ranking 
								best_match = music_path
							end
						end
					end

					if not best_match.nil?
						path = File.expand_path(best_match)
						path = $output_relative_paths? (path.relative_path_from playlist_dir).to_s : path.to_s
						path = path.gsub('/', $dir_separator)
						debug "Match '#{line}' => '#{path}'"
						output_lines.push path
						return
					elsif not tried_alternate_encoding
						# fall back to alternate encoding
						if encoding == 'windows-1252'
							debug "Trying alternate encoding: utf-8 for '#{line}'"
							line = lines[index].force_utf8.strip_utf8
						elsif encoding == 'utf-8'
							debug "Trying alternate encoding: windows-1252 for '#{line}'"
							line = lines[index].force_encoding('windows-1252').encode('utf-8').strip
						else
							raise "Encoding not handled"
						end
						process_line(line, true, local_vars)
					else
						error "no match '#{filename}'  (#{line}), encoding='#{encoding}'"
					end
				end
				process_line(line, false, local_vars)
				$progressbar.increment
			end
		end

		# try multiple file encodings if errors occur
		prefer_utf8 = (input_format == '.m3u8')
		begin
			prevProgress = $progressbar.progress
			if prefer_utf8
				debug "reading playlist as utf-8"
				playlist_lines = File.readlines(input_playlist, :encoding => 'utf-8')
				fix_by_lines(playlist_lines, 'utf-8', local_vars)
			else
				debug "reading playlist as windows-1252"
				playlist_lines = File.readlines(input_playlist, :encoding => 'windows-1252')
				fix_by_lines(playlist_lines, 'windows-1252', local_vars)
			end
		rescue => e1
			begin
				output_lines = []
				$progressbar.progress = prevProgress
				if prefer_utf8
					error "Trying windows-1252 encoding for '#{input_playlist}' .. error='{e1}'"
					playlist_lines = File.readlines(input_playlist, :encoding => 'windows-1252')
					fix_by_lines(playlist_lines, 'windows-1252', local_vars)
				else
					error "Trying utf-8 encoding for '#{input_playlist}' .. error='#{e1}'"
					playlist_lines = File.readlines(input_playlist, :encoding => 'utf-8')
					fix_by_lines(playlist_lines, 'utf-8', local_vars)
				end
			rescue => e2
				raise "ex1: #{e1}\nex2: #{e2}\nex3: Could not read file as either Win-1252 or UTF-8: '#{input_playlist}'"
			end
		end
	end

	File.open(output_playlist, 'w:UTF-8') do |output_file|
		output_lines.each{|line| output_file.puts line }
	end
end




# entry point
input = ARGV[0].gsub('\\','/')

# setup the log file(s)
if File.file? input 
	file_name = File.basename(input, ".*")
	file_dir = File.dirname(File.expand_path(input)) + '/'
	$log_file = $log_file_name
	if $log_file_name_after_playlist
		$log_file = file_name + $output_suffix + '.log'
	end
	$log_file = file_dir + $log_file if $log_file_relative
	$error_file = file_dir + $error_file_name if $log_file_relative
elsif File.directory? input 
	file_dir = input + '/'
	$log_file = $log_file_name
	$log_file = file_dir + $log_file if $log_file_relative
	$error_file = file_dir + $error_file_name if $log_file_relative
end
if $overwrite_log_file and $log_to_file
	File.open($log_file, 'w:UTF-8'){ |file| file.puts '' } if File.exist? $log_file
	if $log_errors_to_separate_file
		File.open($error_file, 'w:UTF-8'){ |file| file.puts '' } if File.exist? $error_file
	end
end

# build the music library
output "building file list..."
$music_list = buildFileList($music_root)
output "finished building file list"

$progressbar = ProgressBar.create( 
	:format => '%a |%b%i| %p%% %e',
	:progress_mark => '=',
	:remainder_mark => ' '
)

# fix the playlists
if File.directory? input 
	output "trying to fix all the playlists in \"#{input}\""
	files = Dir.entries(input).select{|f| 
		File.file? File.join(input, f) and
		$allowed_playlist_types.include? File.extname(f)
	}.map{|f| File.join(input, f)}
	lines = 0
	files.each{|file| lines += File.foreach(file).inject(0) {|c,line| c+1}}
	$progressbar.total = lines
	files.each do |file| 
		fixed_file = fix_playlist(file)
	end
else 
	lines = File.foreach(input).inject(0) {|c,line| c+1}
	$progressbar.total = lines
	fixed_file = fix_playlist(input)
end

output ""
output "log file is   '#{$log_file}'" if $log_to_file and File.exist? $log_file
output "error file is '#{$error_file}'" if $log_to_file and $log_errors_to_separate_file and File.exist? $error_file
output "Done fixing playlists"
_wait = $stdin.gets