require_relative 'config'

# logging functions

def output(args)
	$stdout.puts args
	if $log_to_file
		File.open($debug_log_file, 'a') do |file|
			file.puts args
		end
	end
end

def debug(args)
	if $show_debug
		$stdout.puts args
		$stdout.flush
	end
	if $log_to_file
		File.open($debug_log_file, 'a') do |file|
			file.puts args
		end
	end
end

def debug_error(args)
	if $show_errors
		$stderr.puts args
		$stderr.flush
	end
	if $log_to_file
		File.open($debug_log_file, 'a') do |file|
			file.puts "[STDERR] ".concat(args)
		end
	end
end


# extension functions

class Object
	def blank?
		return true if (self.nil? or self.empty?)
		return self.strip.empty? if defined? self.strip 
		if defined? self.each
			self.each do |obj|
				return false if not obj.blank?
			end
			return true
		end
		return false
	end
end

class String
	def scan_with_error(args)
		res = self.scan(args)
		return res if not res.blank?
		raise RuntimeError, "Scan failed, returned blank result for scan('#{args}') on '#{self}'"
	end
end

def max(*values)
 values.max
end