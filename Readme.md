# tl;dr: Script that can process an entire music library and compress to a specified bitrate, as well as normalize loudness

This script leverages a few ruby libraries and uses ffmpeg for the bulk of its work. All processing is split amongst a specified number of threads (I have it set to 8 threads for example). All of the details that you can adjust are coded as global constant variables at the top of the script.

## Usage

```ruby ffmpeg-script.rb "path-to-top-level-folder-to-normalize"```

Or just ```ruby ffmpeg-script.rb``` and it will use the folder above it as the root. In other words, if the script is inside the folder "Music", the entirety of "Music" will be processed and all of its subfolders.

## What you actually need to know

For now, all options are hard-coded into the script and will need to be edited from within the script (all are set up as global constant variables at the top). Sorry for any inconvenience this might cause, I might make it more user-friendly in the future.

